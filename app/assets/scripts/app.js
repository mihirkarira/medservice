import $ from 'jquery';
import "lazysizes";
import "../styles/styles.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";


//Handles Mobile Menu/Header
let mobileMenu = new MobileMenu();

//Handles Reveal on scroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

//Adding smooth scroll funtionality to aour header links
new SmoothScroll();

//Adding ActiveLinks
new ActiveLinks();

//Adding Modal
new Modal();

if (module.hot) {
    module.hot.accept();
}
